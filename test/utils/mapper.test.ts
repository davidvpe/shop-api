import { Request } from "express";
import { mapRequest } from "../../src/utils/mapper";

describe("Mapper", () => {
  function createMockRequest(data: object = {}): Request {
    return { query: {}, params: {}, ...data } as unknown as Request;
  }

  it("maps a request with no body, query or params", () => {
    const request = createMockRequest();
    const mappedRequest = mapRequest(request);
    expect(mappedRequest).toEqual({
      query: {},
      params: {},
    });
  });

  it("maps a request with a body", () => {
    const request = createMockRequest({ body: { foo: "bar" } });
    const mappedRequest = mapRequest(request);
    expect(mappedRequest).toEqual({
      query: {},
      params: {},
      body: { foo: "bar" },
    });
  });

  it("maps a request with a query", () => {
    const request = createMockRequest({ query: { foo: "bar" } });
    const mappedRequest = mapRequest(request);
    expect(mappedRequest).toEqual({
      query: { foo: "bar" },
      params: {},
    });
  });

  it("maps a request with a params", () => {
    const request = createMockRequest({ params: { foo: "bar" } });
    const mappedRequest = mapRequest(request);
    expect(mappedRequest).toEqual({
      query: {},
      params: { foo: "bar" },
    });
  });
});
