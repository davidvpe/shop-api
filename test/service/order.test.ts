import {
  mock,
  instance,
  when,
  anything,
  anyString,
  anyNumber,
  verify,
  reset,
  capture,
} from "ts-mockito";
import { OrderRepository } from "../../src/repository/order";
import { OrderService } from "../../src/service/order";
import { ProductRepository } from "../../src/repository/product";

describe("Order Service", () => {
  const spyOrderRepository = mock(OrderRepository);
  const spyProductRepository = mock(ProductRepository);

  const orderRepository = instance(spyOrderRepository);
  const productRepository = instance(spyProductRepository);

  const orderService = new OrderService(orderRepository, productRepository);

  function prepareMocks() {
    when(spyOrderRepository.add(anything())).thenResolve({
      id: "1",
      paymentMethod: "credit-card",
      userAddress: "address",
      userLat: 15.5,
      userLng: 15.5,
      userName: "David Velarde",
      userPhone: "999999999",
      userPhoto: "superphoto",
      ProductToOrder: [
        {
          productId: "1",
          amount: 10,
        },
        {
          productId: "2",
          amount: 10,
        },
      ],
    });

    when(spyOrderRepository.get("1")).thenResolve({
      id: "1",

      paymentMethod: "credit-card",
      userAddress: "address",
      userLat: 15.5,
      userLng: 15.5,
      userName: "David Velarde",
      userPhone: "999999999",
      userPhoto: "superphoto",
      ProductToOrder: [
        {
          productId: "1",
          amount: 10,
        },
        {
          productId: "2",
          amount: 10,
        },
      ],
    });

    when(
      spyProductRepository.updateStock(anyString(), anyNumber())
    ).thenResolve(anything());
  }

  beforeEach(() => {
    reset(spyOrderRepository);
    reset(spyProductRepository);
  });

  it("should create order", async () => {
    prepareMocks();

    const order = await orderService.createOrder({
      products: [
        {
          id: "1",
          amount: 10,
        },
        {
          id: "2",
          amount: 10,
        },
      ],

      paymentMethod: "credit-card",
      userAddress: "address",
      userLat: 15.5,
      userLng: 15.5,
      userName: "David Velarde",
      userPhone: "999999999",
      userPhoto: "superphoto",
    });

    expect(order).toBeDefined();

    verify(spyOrderRepository.add(anything())).once();
    verify(spyProductRepository.updateStock("1", -10)).once();
    verify(spyProductRepository.updateStock("2", -10)).once();
  });
  it("should retrieve order 1", async () => {
    prepareMocks();

    const order = await orderService.getOrder("1");

    expect(order).toBeDefined();

    verify(spyOrderRepository.get("1")).once();
  });

  describe("should handle pagination when listing", () => {
    beforeEach(() => {
      reset(spyOrderRepository);
      when(spyOrderRepository.list(anyNumber(), anyNumber())).thenCall(
        (...args) => {
          const orders = [...Array(100)].map((_, i) => ({
            id: i.toString(),
            status: "payment-pending",
            ProductToOrder: [
              {
                productId: "1",
                amount: 10,
              },
              {
                productId: "2",
                amount: 10,
              },
            ],
          }));

          let [page, pageSize] = args;

          page = page || 0;
          pageSize = pageSize || 10;

          return orders.slice(page * pageSize, (page + 1) * pageSize);
        }
      );
    });
    it("should retrieve orders paginated when pageSize is missing", async () => {
      const orders = await orderService.getOrders({
        page: "10",
      });

      expect(orders).toHaveLength(10);
      expect(orders[0].id).toEqual("90");
      expect(orders[9].id).toEqual("99");
    });

    it("should retrieve orders paginated when page is missing", async () => {
      const orders = await orderService.getOrders({
        pageSize: "4",
      });

      expect(orders).toHaveLength(4);
      expect(orders[0].id).toEqual("0");
      expect(orders[3].id).toEqual("3");
    });

    it("should retrieve orders paginated when empty pagination object is provided", async () => {
      const orders = await orderService.getOrders({});

      expect(orders).toHaveLength(10);
      expect(orders[0].id).toEqual("0");
      expect(orders[9].id).toEqual("9");
    });

    it("should retrieve orders paginated when no pagination object is provided", async () => {
      const orders = await orderService.getOrders();

      expect(orders).toHaveLength(10);
      expect(orders[0].id).toEqual("0");
      expect(orders[9].id).toEqual("9");
    });
  });
});
