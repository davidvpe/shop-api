import * as Pagination from "../../src/model/pagination";

describe("Pagination model", () => {
  it("maps pagination when no query is defined", () => {
    const pagination = Pagination.parsePaginationQuery({});
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
  it("maps pagination when query is defined", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1",
      pageSize: "10",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
  it("maps pagination when pageSize is defined and invalid", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1",
      pageSize: "10a",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });

  it("maps pagination when pageSize is defined and invalid", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1",
      pageSize: "-10",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
  it("maps pagination when pageSize is defined and invalid", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1",
      pageSize: "0",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });

  it("maps pagination when page is defined and invalid", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1a",
      pageSize: "10",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
  it("maps pagination when page is defined and invalid", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "-2",
      pageSize: "10",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
  it("maps pagination when query is defined and page is missing", () => {
    const pagination = Pagination.parsePaginationQuery({
      pageSize: "15",
    });
    expect(pagination).toEqual({
      pageSize: 15,
      page: 0,
    });
  });
  it("maps pagination when query is defined and pageSize is missing", () => {
    const pagination = Pagination.parsePaginationQuery({
      page: "1",
    });
    expect(pagination).toEqual({
      page: 0,
      pageSize: 10,
    });
  });
});
