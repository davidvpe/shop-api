"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.orderRepositoryInstance = exports.OrderRepository = void 0;
const db_1 = __importDefault(require("../utils/db"));
class OrderRepository {
    dbClient;
    constructor(dbClient = db_1.default) {
        this.dbClient = dbClient;
    }
    async add(order) {
        const { products, ...rest } = order;
        return this.dbClient.order.create({
            data: {
                ...rest,
                ProductToOrder: {
                    create: order.products.map((product) => ({
                        productId: product.id,
                        amount: product.amount,
                    })),
                },
            },
            include: {
                ProductToOrder: {
                    select: {
                        amount: true,
                        productId: true,
                    },
                },
            },
        });
    }
    async delete(id) {
        return this.dbClient.order.delete({
            where: {
                id,
            },
        });
    }
    // async update(id: string, order: Partial<PreOrderModel>) {
    //   return this.dbClient.order.update({
    //     where: {
    //       id,
    //     },
    //     data: order,
    //     include: {
    //       ProductToOrder: {
    //         select: {
    //           amount: true,
    //           productId: true,
    //         },
    //       },
    //     },
    //   });
    // }
    async get(id) {
        return this.dbClient.order.findUnique({
            where: {
                id,
            },
            include: {
                ProductToOrder: {
                    select: {
                        amount: true,
                        productId: true,
                    },
                },
            },
        });
    }
    async list(page, pageSize) {
        return this.dbClient.order.findMany({
            skip: page * pageSize,
            take: pageSize,
        });
    }
}
exports.OrderRepository = OrderRepository;
exports.orderRepositoryInstance = new OrderRepository();
