"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productRepositoryInstance = exports.ProductRepository = void 0;
const db_1 = __importDefault(require("../utils/db"));
class ProductRepository {
    dbClient;
    constructor(dbClient = db_1.default) {
        this.dbClient = dbClient;
    }
    async add(product) {
        return this.dbClient.product.create({
            data: product,
        });
    }
    async delete(id) {
        return this.dbClient.product.delete({
            where: {
                id,
            },
        });
    }
    async update(id, product) {
        return this.dbClient.product.update({
            where: {
                id,
            },
            data: product,
        });
    }
    async get(id) {
        return this.dbClient.product.findUnique({
            where: {
                id,
            },
        });
    }
    async list(page, pageSize) {
        return this.dbClient.product.findMany({
            skip: page * pageSize,
            take: pageSize,
        });
    }
    async updateStock(id, delta) {
        return this.dbClient.product.update({
            where: {
                id,
            },
            data: {
                stock: {
                    increment: delta,
                },
            },
        });
    }
}
exports.ProductRepository = ProductRepository;
exports.productRepositoryInstance = new ProductRepository();
