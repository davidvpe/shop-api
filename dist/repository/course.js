"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.courseRepositoryInstance = exports.CourseRepository = void 0;
const db_1 = __importDefault(require("../utils/db"));
class CourseRepository {
    dbClient;
    constructor(dbClient = db_1.default) {
        this.dbClient = dbClient;
    }
    async add(course) {
        return this.dbClient.course.create({
            data: course,
        });
    }
    async delete(id) {
        return this.dbClient.course.delete({
            where: {
                id,
            },
        });
    }
    async update(id, course) {
        return this.dbClient.course.update({
            where: {
                id,
            },
            data: course,
        });
    }
    async get(id) {
        return this.dbClient.course.findUnique({
            where: {
                id,
            },
        });
    }
    async list(page, pageSize) {
        return this.dbClient.course.findMany({
            skip: page * pageSize,
            take: pageSize,
        });
    }
}
exports.CourseRepository = CourseRepository;
exports.courseRepositoryInstance = new CourseRepository();
