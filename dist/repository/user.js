"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRepositoryInstance = exports.UserRepository = void 0;
const db_1 = __importDefault(require("../utils/db"));
class UserRepository {
    dbClient;
    constructor(dbClient = db_1.default) {
        this.dbClient = dbClient;
    }
    async add(user) {
        return this.dbClient.user.create({
            data: {
                dob: new Date(user.dob),
                email: user.email,
                name: user.name,
                photo: user.photo,
                address: user.address,
                latitude: user.latitude,
                longitude: user.longitude,
            },
        });
    }
    async delete(id) {
        return this.dbClient.user.delete({
            where: {
                id,
            },
        });
    }
    async update(id, user) {
        return this.dbClient.user.update({
            where: {
                id,
            },
            data: {
                ...(user.dob ? { dob: new Date(user.dob) } : {}),
                ...(user.email ? { email: user.email } : {}),
                ...(user.name ? { name: user.name } : {}),
                ...(user.photo ? { photo: user.photo } : {}),
                ...(user.latitude ? { latitude: user.latitude } : {}),
                ...(user.longitude ? { longitude: user.longitude } : {}),
            },
            select: {
                id: true,
            },
        });
    }
    async get(id) {
        return this.dbClient.user.findUnique({
            where: {
                id,
            },
        });
    }
    async list(page, pageSize) {
        return this.dbClient.user.findMany({
            skip: page * pageSize,
            take: pageSize,
        });
    }
}
exports.UserRepository = UserRepository;
exports.userRepositoryInstance = new UserRepository();
