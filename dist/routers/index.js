"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.user = exports.course = exports.order = exports.payment = exports.product = void 0;
const product_1 = __importDefault(require("./product"));
exports.product = product_1.default;
const payment_1 = __importDefault(require("./payment"));
exports.payment = payment_1.default;
const order_1 = __importDefault(require("./order"));
exports.order = order_1.default;
const course_1 = __importDefault(require("./course"));
exports.course = course_1.default;
const user_1 = __importDefault(require("./user"));
exports.user = user_1.default;
