"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const order_1 = require("../service/order");
const mapper_1 = require("../utils/mapper");
const router = (0, express_1.Router)();
router.post("/", (0, mapper_1.secureRequest)(async ({ res, body }) => {
    const result = await order_1.orderServiceInstance.createOrder(body);
    return res.status(201).json(result);
}));
router.get("/", (0, mapper_1.secureRequest)(async ({ res, query }) => {
    const result = await order_1.orderServiceInstance.getOrders(query);
    return res.status(200).json(result);
}));
router.get("/:id", (0, mapper_1.secureRequest)(async ({ res, params }) => {
    const result = await order_1.orderServiceInstance.getOrder(params.id);
    if (result === null) {
        return res.status(404).json({ message: "Product not found" });
    }
    return res.status(200).json(result);
}));
exports.default = router;
