"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const user_1 = require("../service/user");
const mapper_1 = require("../utils/mapper");
const router = (0, express_1.Router)();
router.post("/", (0, mapper_1.secureRequest)(async ({ res, body }) => {
    const result = await user_1.userServiceInstance.createUser(body);
    return res.status(201).json(result);
}));
router.patch("/:id", (0, mapper_1.secureRequest)(async ({ res, body, params }) => {
    const result = await user_1.userServiceInstance.updateUser(params.id, body);
    return res.status(200).json(result);
}));
router.get("/", (0, mapper_1.secureRequest)(async ({ res, query }) => {
    const result = await user_1.userServiceInstance.getUsers(query);
    return res.status(200).json(result);
}));
router.get("/:id", (0, mapper_1.secureRequest)(async ({ res, params }) => {
    const result = await user_1.userServiceInstance.getUser(params.id);
    if (result === null) {
        return res.status(404).json({ message: "User not found" });
    }
    return res.status(200).json(result);
}));
// router.delete(
//   "/:id",
//   secureRequest(async ({ res, params }) => {
//     const result = await userServiceInstance.deleteUser(params.id);
//     return res.status(200).json(result);
//   })
// );
exports.default = router;
