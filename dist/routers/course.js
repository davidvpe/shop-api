"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const course_1 = require("../service/course");
const mapper_1 = require("../utils/mapper");
const router = (0, express_1.Router)();
router.post("/", (0, mapper_1.secureRequest)(async ({ res, body }) => {
    const result = await course_1.courseServiceInstance.createCourse(body);
    return res.status(201).json(result);
}));
router.patch("/:id", (0, mapper_1.secureRequest)(async ({ res, body, params }) => {
    const result = await course_1.courseServiceInstance.updateCourse(params.id, body);
    return res.status(200).json(result);
}));
router.get("/", (0, mapper_1.secureRequest)(async ({ res, query }) => {
    const result = await course_1.courseServiceInstance.getCourses(query);
    return res.status(200).json(result);
}));
router.get("/:id", (0, mapper_1.secureRequest)(async ({ res, params }) => {
    const result = await course_1.courseServiceInstance.getCourse(params.id);
    if (result === null) {
        return res.status(404).json({ message: "Course not found" });
    }
    return res.status(200).json(result);
}));
router.delete("/:id", (0, mapper_1.secureRequest)(async ({ res, params }) => {
    const result = await course_1.courseServiceInstance.deleteCourse(params.id);
    return res.status(200).json(result);
}));
exports.default = router;
