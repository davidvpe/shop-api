"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchUserValidator = exports.UserValidator = void 0;
const zod_1 = require("zod");
exports.UserValidator = zod_1.z.object({
    name: zod_1.z.string(),
    photo: zod_1.z
        .string()
        .max(1048576, "Size too large. Max 1 Mb")
        .min(1024, "Size too little. Min 1 Kb"),
    dob: zod_1.z.number().max(1698347684210),
    email: zod_1.z.string().email(),
    address: zod_1.z.string(),
    latitude: zod_1.z.number(),
    longitude: zod_1.z.number(),
});
exports.PatchUserValidator = exports.UserValidator.partial();
