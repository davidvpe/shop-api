"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchCourseValidator = exports.CourseValidator = void 0;
const zod_1 = require("zod");
exports.CourseValidator = zod_1.z.object({
    name: zod_1.z.string(),
    category: zod_1.z.enum(["cloud", "mobile", "web"]),
});
exports.PatchCourseValidator = exports.CourseValidator.partial();
