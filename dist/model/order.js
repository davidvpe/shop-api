"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchOrderValidator = exports.OrderValidator = void 0;
const zod_1 = require("zod");
exports.OrderValidator = zod_1.z.object({
    products: zod_1.z
        .array(zod_1.z.object({
        id: zod_1.z.string().min(1).max(100),
        amount: zod_1.z.number().positive().int(),
    }))
        .min(1)
        .max(100),
    paymentMethod: zod_1.z.enum(["cash", "credit-card", "debit-card"]),
    userName: zod_1.z.string().min(1).max(100),
    userPhone: zod_1.z.string().min(1).max(9),
    userAddress: zod_1.z.string().min(1).max(100),
    userLat: zod_1.z.number(),
    userLng: zod_1.z.number(),
    userPhoto: zod_1.z.string(),
});
exports.PatchOrderValidator = exports.OrderValidator.partial();
