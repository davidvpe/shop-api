"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PatchProductValidator = exports.PreProductValidator = exports.CreateProductValidator = void 0;
const zod_1 = require("zod");
exports.CreateProductValidator = zod_1.z.object({
    name: zod_1.z.string().min(1).max(100),
    price: zod_1.z.number().min(0),
    description: zod_1.z.string().min(1).max(500),
    stock: zod_1.z.number().min(1).max(1000),
});
exports.PreProductValidator = exports.CreateProductValidator.extend({
    slug: zod_1.z.string().min(1).max(100),
});
exports.PatchProductValidator = exports.PreProductValidator.partial();
