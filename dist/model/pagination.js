"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.parsePaginationQuery = exports.PaginationQueryValidator = void 0;
const zod_1 = require("zod");
exports.PaginationQueryValidator = zod_1.z.object({
    page: zod_1.z.string().optional(),
    pageSize: zod_1.z.string().optional(),
});
const parsePaginationQuery = (query) => {
    const { page, pageSize } = exports.PaginationQueryValidator.parse(query);
    let validatedPage = 1;
    if (page) {
        validatedPage = Number(page);
        if (isNaN(validatedPage) || validatedPage <= 0)
            validatedPage = 1;
    }
    let validatedPageSize = 10;
    if (pageSize) {
        validatedPageSize = Number(pageSize);
        if (isNaN(validatedPageSize) ||
            validatedPageSize <= 0 ||
            validatedPageSize > 10)
            validatedPageSize = 10;
    }
    return { page: validatedPage - 1, pageSize: validatedPageSize };
};
exports.parsePaginationQuery = parsePaginationQuery;
