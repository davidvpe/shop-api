"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.narrowOrThrow = void 0;
function narrowOrThrow(name) {
    if (name in process.env) {
        return process.env[name];
    }
    else {
        throw new Error(`${name} not found in env`);
    }
}
exports.narrowOrThrow = narrowOrThrow;
