"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.secureRequest = exports.mapRequest = void 0;
const library_1 = require("@prisma/client/runtime/library");
const zod_1 = require("zod");
const mapRequest = (req) => {
    return {
        body: req.body,
        query: Object.entries(req.query)
            .filter(([, value]) => value)
            .reduce((curr, [key, value]) => ({
            ...curr,
            [key]: value.toString(),
        }), {}),
        params: req.params,
    };
};
exports.mapRequest = mapRequest;
const secureRequest = (clean) => (req, res, next) => {
    const { body, query, params } = (0, exports.mapRequest)(req);
    return clean({ query, params, res, body })
        .then(() => next())
        .catch((err) => {
        console.error("============= ERROR ==============");
        console.error("Internal error", err);
        console.error("============= ERROR ==============");
        console.error("Request body error", req.body);
        console.error("Request query error", req.query);
        console.error("Request params error", req.params);
        if (err instanceof zod_1.ZodError) {
            return res.status(400).json({
                errors: err.issues.map((issue, idx) => ({
                    path: issue.path.join(" -> "),
                    error: issue.message,
                })),
            });
        }
        if (err instanceof library_1.PrismaClientKnownRequestError) {
            return res
                .status(400)
                .json({ message: err.meta?.cause || err.message });
        }
        return res.status(500).json({ message: "Internal Server Error" });
    });
};
exports.secureRequest = secureRequest;
