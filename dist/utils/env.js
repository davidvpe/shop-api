"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnvironmentManager = void 0;
const dotenv_1 = __importDefault(require("dotenv"));
const narrow_1 = require("./narrow");
class Environment {
    port;
    constructor() {
        dotenv_1.default.config();
        this.port = (0, narrow_1.narrowOrThrow)("PORT");
    }
}
const EnvironmentManager = new Environment();
exports.EnvironmentManager = EnvironmentManager;
