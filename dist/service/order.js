"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.orderServiceInstance = exports.OrderService = void 0;
const pagination_1 = require("../model/pagination");
const order_1 = require("../repository/order");
const order_2 = require("../model/order");
const product_1 = require("../repository/product");
class OrderService {
    orderRepository;
    productRepository;
    constructor(orderRepository = order_1.orderRepositoryInstance, productRepository = product_1.productRepositoryInstance) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }
    async getOrders(pagination) {
        const { page, pageSize } = (0, pagination_1.parsePaginationQuery)(pagination || {});
        return this.orderRepository.list(page, pageSize);
    }
    async getOrder(id) {
        return this.orderRepository.get(id);
    }
    async createOrder(request) {
        const order = order_2.OrderValidator.parse(request);
        const preOrder = await this.orderRepository.add(order);
        // this.updateOrderProducts(order.products);
        return preOrder;
    }
    // async updateOrder(id: string, request: unknown) {
    //   const order = PatchOrderValidator.parse(request);
    //   if (order.products) {
    //     await this.updateOrderProducts(id, order.products, true);
    //   }
    //   return this.orderRepository.update(id, order);
    // }
    async updateOrderProducts(
    // orderId: string,
    newProducts
    // isUpdate: boolean
    ) {
        // const order = await this.orderRepository.get(orderId);
        // if (!order) return;
        // if (isUpdate) {
        //   const oldProducts = order.ProductToOrder;
        //   const oldProductUpdates = oldProducts.map(async (product) =>
        //     this.productRepository.updateStock(product.productId, product.amount)
        //   );
        //   await Promise.all(oldProductUpdates);
        // }
        const stockUpdates = newProducts.map(async (product) => this.productRepository.updateStock(product.id, product.amount * -1));
        await Promise.all(stockUpdates);
    }
}
exports.OrderService = OrderService;
exports.orderServiceInstance = new OrderService();
