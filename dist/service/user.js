"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.userServiceInstance = exports.UserService = void 0;
const pagination_1 = require("../model/pagination");
const user_1 = require("../repository/user");
const user_2 = require("../model/user");
class UserService {
    userRepository;
    constructor(userRepository = user_1.userRepositoryInstance) {
        this.userRepository = userRepository;
    }
    async getUsers(pagination) {
        const { page, pageSize } = (0, pagination_1.parsePaginationQuery)(pagination || {});
        return this.userRepository.list(page, pageSize);
    }
    async getUser(id) {
        return this.userRepository.get(id);
    }
    async createUser(request) {
        const user = user_2.UserValidator.parse(request);
        return this.userRepository.add({
            ...user,
        });
    }
    async updateUser(id, request) {
        const user = user_2.PatchUserValidator.parse(request);
        return this.userRepository.update(id, user);
    }
    async deleteUser(id) {
        return this.userRepository.delete(id);
    }
}
exports.UserService = UserService;
exports.userServiceInstance = new UserService();
