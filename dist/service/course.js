"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.courseServiceInstance = exports.CourseService = void 0;
const pagination_1 = require("../model/pagination");
const course_1 = require("../repository/course");
const course_2 = require("../model/course");
class CourseService {
    courseRepository;
    constructor(courseRepository = course_1.courseRepositoryInstance) {
        this.courseRepository = courseRepository;
    }
    async getCourses(pagination) {
        const { page, pageSize } = (0, pagination_1.parsePaginationQuery)(pagination || {});
        return this.courseRepository.list(page, pageSize);
    }
    async getCourse(id) {
        return this.courseRepository.get(id);
    }
    async createCourse(request) {
        const course = course_2.CourseValidator.parse(request);
        return this.courseRepository.add({
            ...course,
        });
    }
    async updateCourse(id, request) {
        const course = course_2.PatchCourseValidator.parse(request);
        return this.courseRepository.update(id, course);
    }
    async deleteCourse(id) {
        return this.courseRepository.delete(id);
    }
}
exports.CourseService = CourseService;
exports.courseServiceInstance = new CourseService();
