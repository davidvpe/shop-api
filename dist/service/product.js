"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productServiceInstance = exports.ProductService = void 0;
const slugify_1 = __importDefault(require("slugify"));
const pagination_1 = require("../model/pagination");
const product_1 = require("../model/product");
const product_2 = require("../repository/product");
class ProductService {
    productRepository;
    constructor(productRepository = product_2.productRepositoryInstance) {
        this.productRepository = productRepository;
    }
    async getProducts(pagination) {
        const { page, pageSize } = (0, pagination_1.parsePaginationQuery)(pagination || {});
        return this.productRepository.list(page, pageSize);
    }
    async getProduct(id) {
        return this.productRepository.get(id);
    }
    async createProduct(request) {
        const product = product_1.CreateProductValidator.parse(request);
        const slug = (0, slugify_1.default)(`${product.name}-${new Date().getTime()}`).toLowerCase();
        return this.productRepository.add({
            ...product,
            slug,
        });
    }
    async updateProduct(id, request) {
        const product = product_1.PatchProductValidator.parse(request);
        return this.productRepository.update(id, product);
    }
}
exports.ProductService = ProductService;
exports.productServiceInstance = new ProductService();
