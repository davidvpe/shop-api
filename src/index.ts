import express, { Express, NextFunction, Request, Response } from "express";
import dotenv from "dotenv";

import { EnvironmentManager } from "./utils/env";
import * as Routes from "./routers";

dotenv.config();

const app: Express = express();

app.use(
  express.json({
    limit: "4mb",
  })
);

const port = EnvironmentManager.port;

app.use("/product", Routes.product);
app.use("/payment", Routes.payment);
app.use("/order", Routes.order);
app.use("/course", Routes.course);
app.use("/user", Routes.user);

app.listen(port, () => {
  console.log(`⚡️ [server]: Server is running at http://localhost:${port}`);
});

export default app;
