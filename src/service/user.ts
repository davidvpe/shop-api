import { parsePaginationQuery } from "../model/pagination";
import { UserRepository, userRepositoryInstance } from "../repository/user";
import { UserValidator, PatchUserValidator } from "../model/user";

export class UserService {
  constructor(
    private userRepository: UserRepository = userRepositoryInstance
  ) {}

  async getUsers(pagination?: Record<string, string>) {
    const { page, pageSize } = parsePaginationQuery(pagination || {});
    return this.userRepository.list(page, pageSize);
  }

  async getUser(id: string) {
    return this.userRepository.get(id);
  }

  async createUser(request: unknown) {
    const user = UserValidator.parse(request);
    return this.userRepository.add({
      ...user,
    });
  }

  async updateUser(id: string, request: unknown) {
    const user = PatchUserValidator.parse(request);
    return this.userRepository.update(id, user);
  }

  async deleteUser(id: string) {
    return this.userRepository.delete(id);
  }
}

export const userServiceInstance = new UserService();
