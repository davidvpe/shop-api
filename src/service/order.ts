import { parsePaginationQuery } from "../model/pagination";

import { OrderRepository, orderRepositoryInstance } from "../repository/order";
import { OrderValidator, PatchOrderValidator } from "../model/order";
import {
  ProductRepository,
  productRepositoryInstance,
} from "../repository/product";

export class OrderService {
  constructor(
    private orderRepository: OrderRepository = orderRepositoryInstance,
    private productRepository: ProductRepository = productRepositoryInstance
  ) {}

  async getOrders(pagination?: Record<string, string>) {
    const { page, pageSize } = parsePaginationQuery(pagination || {});

    return this.orderRepository.list(page, pageSize);
  }

  async getOrder(id: string) {
    return this.orderRepository.get(id);
  }

  async createOrder(request: unknown) {
    const order = OrderValidator.parse(request);

    const preOrder = await this.orderRepository.add(order);

    // this.updateOrderProducts(order.products);

    return preOrder;
  }

  // async updateOrder(id: string, request: unknown) {
  //   const order = PatchOrderValidator.parse(request);
  //   if (order.products) {
  //     await this.updateOrderProducts(id, order.products, true);
  //   }
  //   return this.orderRepository.update(id, order);
  // }

  async updateOrderProducts(
    // orderId: string,
    newProducts: { id: string; amount: number }[]
    // isUpdate: boolean
  ) {
    // const order = await this.orderRepository.get(orderId);

    // if (!order) return;

    // if (isUpdate) {
    //   const oldProducts = order.ProductToOrder;
    //   const oldProductUpdates = oldProducts.map(async (product) =>
    //     this.productRepository.updateStock(product.productId, product.amount)
    //   );
    //   await Promise.all(oldProductUpdates);
    // }

    const stockUpdates = newProducts.map(async (product) =>
      this.productRepository.updateStock(product.id, product.amount * -1)
    );

    await Promise.all(stockUpdates);
  }
}

export const orderServiceInstance = new OrderService();
