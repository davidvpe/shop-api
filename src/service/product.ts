import slugify from "slugify";
import { parsePaginationQuery } from "../model/pagination";
import {
  CreateProductValidator,
  PatchProductValidator,
} from "../model/product";
import {
  ProductRepository,
  productRepositoryInstance,
} from "../repository/product";

export class ProductService {
  constructor(
    private productRepository: ProductRepository = productRepositoryInstance
  ) {}

  async getProducts(pagination?: Record<string, string>) {
    const { page, pageSize } = parsePaginationQuery(pagination || {});
    return this.productRepository.list(page, pageSize);
  }

  async getProduct(id: string) {
    return this.productRepository.get(id);
  }

  async createProduct(request: unknown) {
    const product = CreateProductValidator.parse(request);

    const slug = slugify(
      `${product.name}-${new Date().getTime()}`
    ).toLowerCase();

    return this.productRepository.add({
      ...product,
      slug,
    });
  }

  async updateProduct(id: string, request: unknown) {
    const product = PatchProductValidator.parse(request);
    return this.productRepository.update(id, product);
  }
}

export const productServiceInstance = new ProductService();
