import { parsePaginationQuery } from "../model/pagination";
import {
  CourseRepository,
  courseRepositoryInstance,
} from "../repository/course";
import { CourseValidator, PatchCourseValidator } from "../model/course";

export class CourseService {
  constructor(
    private courseRepository: CourseRepository = courseRepositoryInstance
  ) {}

  async getCourses(pagination?: Record<string, string>) {
    const { page, pageSize } = parsePaginationQuery(pagination || {});
    return this.courseRepository.list(page, pageSize);
  }

  async getCourse(id: string) {
    return this.courseRepository.get(id);
  }

  async createCourse(request: unknown) {
    const course = CourseValidator.parse(request);
    return this.courseRepository.add({
      ...course,
    });
  }

  async updateCourse(id: string, request: unknown) {
    const course = PatchCourseValidator.parse(request);
    return this.courseRepository.update(id, course);
  }

  async deleteCourse(id: string) {
    return this.courseRepository.delete(id);
  }
}

export const courseServiceInstance = new CourseService();
