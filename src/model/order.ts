import { z } from "zod";

export const OrderValidator = z.object({
  products: z
    .array(
      z.object({
        id: z.string().min(1).max(100),
        amount: z.number().positive().int(),
      })
    )
    .min(1)
    .max(100),
  paymentMethod: z.enum(["cash", "credit-card", "debit-card"]),
  userName: z.string().min(1).max(100),
  userPhone: z.string().min(1).max(9),
  userAddress: z.string().min(1).max(100),
  userLat: z.number(),
  userLng: z.number(),
  userPhoto: z.string(),
});

export const PatchOrderValidator = OrderValidator.partial();

export type OrderModel = z.infer<typeof OrderValidator>;
