import { z } from "zod";

export const CreateProductValidator = z.object({
  name: z.string().min(1).max(100),
  price: z.number().min(0),
  description: z.string().min(1).max(500),
  stock: z.number().min(1).max(1000),
});

export type CreateProductModel = z.infer<typeof CreateProductValidator>;

export const PreProductValidator = CreateProductValidator.extend({
  slug: z.string().min(1).max(100),
});

export const PatchProductValidator = PreProductValidator.partial();

export type PreProductModel = z.infer<typeof PreProductValidator>;
export type PatchProductModel = z.infer<typeof PatchProductValidator>;
