import { z } from "zod";

export const UserValidator = z.object({
  name: z.string(),
  photo: z
    .string()
    .max(1048576, "Size too large. Max 1 Mb")
    .min(1024, "Size too little. Min 1 Kb"),
  dob: z.number().max(1698347684210),
  email: z.string().email(),
  address: z.string(),
  latitude: z.number(),
  longitude: z.number(),
});

export const PatchUserValidator = UserValidator.partial();

export type UserModel = z.infer<typeof UserValidator>;

export type PatchUserModel = z.infer<typeof PatchUserValidator>;
