import { z } from "zod";

export const PaginationQueryValidator = z.object({
  page: z.string().optional(),
  pageSize: z.string().optional(),
});

export type PaginationQuery = z.infer<typeof PaginationQueryValidator>;

export const parsePaginationQuery = (query: Record<string, string>) => {
  const { page, pageSize } = PaginationQueryValidator.parse(query);

  let validatedPage = 1;

  if (page) {
    validatedPage = Number(page);
    if (isNaN(validatedPage) || validatedPage <= 0) validatedPage = 1;
  }

  let validatedPageSize = 10;

  if (pageSize) {
    validatedPageSize = Number(pageSize);
    if (
      isNaN(validatedPageSize) ||
      validatedPageSize <= 0 ||
      validatedPageSize > 10
    )
      validatedPageSize = 10;
  }

  return { page: validatedPage - 1, pageSize: validatedPageSize };
};
