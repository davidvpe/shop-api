import { z } from "zod";

export const CourseValidator = z.object({
  name: z.string(),
  category: z.enum(["cloud", "mobile", "web"]),
});

export const PatchCourseValidator = CourseValidator.partial();

export type CourseModel = z.infer<typeof CourseValidator>;

export type PatchCourseModel = z.infer<typeof PatchCourseValidator>;
