import { Router } from "express";
import { ProductService, productServiceInstance } from "../service/product";
import { mapRequest, secureRequest } from "../utils/mapper";

const router = Router();

router.post(
  "/",
  secureRequest(async ({ res, body }) => {
    const result = await productServiceInstance.createProduct(body);
    return res.status(201).json(result);
  })
);

router.patch(
  "/:id",
  secureRequest(async ({ res, body, params }) => {
    const result = await productServiceInstance.updateProduct(params.id, body);
    return res.status(200).json(result);
  })
);

router.get(
  "/",
  secureRequest(async ({ res, query }) => {
    const result = await productServiceInstance.getProducts(query);
    return res.status(200).json(result);
  })
);

router.get(
  "/:id",
  secureRequest(async ({ res, params }) => {
    const result = await productServiceInstance.getProduct(params.id);
    if (result === null) {
      return res.status(404).json({ message: "Product not found" });
    }
    return res.status(200).json(result);
  })
);

export default router;
