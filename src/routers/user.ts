import { Router } from "express";
import { userServiceInstance } from "../service/user";
import { secureRequest } from "../utils/mapper";

const router = Router();

router.post(
  "/",
  secureRequest(async ({ res, body }) => {
    const result = await userServiceInstance.createUser(body);
    return res.status(201).json(result);
  })
);

router.patch(
  "/:id",
  secureRequest(async ({ res, body, params }) => {
    const result = await userServiceInstance.updateUser(params.id, body);
    return res.status(200).json(result);
  })
);

router.get(
  "/",
  secureRequest(async ({ res, query }) => {
    const result = await userServiceInstance.getUsers(query);
    return res.status(200).json(result);
  })
);

router.get(
  "/:id",
  secureRequest(async ({ res, params }) => {
    const result = await userServiceInstance.getUser(params.id);
    if (result === null) {
      return res.status(404).json({ message: "User not found" });
    }
    return res.status(200).json(result);
  })
);

// router.delete(
//   "/:id",
//   secureRequest(async ({ res, params }) => {
//     const result = await userServiceInstance.deleteUser(params.id);
//     return res.status(200).json(result);
//   })
// );

export default router;
