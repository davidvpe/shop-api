import { Router } from "express";
import { courseServiceInstance } from "../service/course";
import { secureRequest } from "../utils/mapper";

const router = Router();

router.post(
  "/",
  secureRequest(async ({ res, body }) => {
    const result = await courseServiceInstance.createCourse(body);
    return res.status(201).json(result);
  })
);

router.patch(
  "/:id",
  secureRequest(async ({ res, body, params }) => {
    const result = await courseServiceInstance.updateCourse(params.id, body);
    return res.status(200).json(result);
  })
);

router.get(
  "/",
  secureRequest(async ({ res, query }) => {
    const result = await courseServiceInstance.getCourses(query);
    return res.status(200).json(result);
  })
);

router.get(
  "/:id",
  secureRequest(async ({ res, params }) => {
    const result = await courseServiceInstance.getCourse(params.id);
    if (result === null) {
      return res.status(404).json({ message: "Course not found" });
    }
    return res.status(200).json(result);
  })
);

router.delete(
  "/:id",
  secureRequest(async ({ res, params }) => {
    const result = await courseServiceInstance.deleteCourse(params.id);
    return res.status(200).json(result);
  })
);

export default router;
