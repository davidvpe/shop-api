import { Router } from "express";
import { orderServiceInstance } from "../service/order";
import { secureRequest } from "../utils/mapper";

const router = Router();

router.post(
  "/",
  secureRequest(async ({ res, body }) => {
    const result = await orderServiceInstance.createOrder(body);
    return res.status(201).json(result);
  })
);

router.get(
  "/",
  secureRequest(async ({ res, query }) => {
    const result = await orderServiceInstance.getOrders(query);
    return res.status(200).json(result);
  })
);

router.get(
  "/:id",
  secureRequest(async ({ res, params }) => {
    const result = await orderServiceInstance.getOrder(params.id);
    if (result === null) {
      return res.status(404).json({ message: "Product not found" });
    }
    return res.status(200).json(result);
  })
);

export default router;
