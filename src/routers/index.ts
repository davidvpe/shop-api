import productRouter from "./product";
import paymentRouter from "./payment";
import orderRouter from "./order";
import courseRouter from "./course";
import userRouter from "./user";

export {
  productRouter as product,
  paymentRouter as payment,
  orderRouter as order,
  courseRouter as course,
  userRouter as user,
};
