import defaultPrismaClient from "../utils/db";
import { PrismaClient } from "@prisma/client";
import { OrderModel } from "../model/order";

export class OrderRepository {
  constructor(private dbClient: PrismaClient = defaultPrismaClient) {}

  async add(order: OrderModel) {
    const { products, ...rest } = order;
    return this.dbClient.order.create({
      data: {
        ...rest,
        ProductToOrder: {
          create: order.products.map((product) => ({
            productId: product.id,
            amount: product.amount,
          })),
        },
      },
      include: {
        ProductToOrder: {
          select: {
            amount: true,
            productId: true,
          },
        },
      },
    });
  }

  async delete(id: string) {
    return this.dbClient.order.delete({
      where: {
        id,
      },
    });
  }

  // async update(id: string, order: Partial<PreOrderModel>) {
  //   return this.dbClient.order.update({
  //     where: {
  //       id,
  //     },
  //     data: order,
  //     include: {
  //       ProductToOrder: {
  //         select: {
  //           amount: true,
  //           productId: true,
  //         },
  //       },
  //     },
  //   });
  // }

  async get(id: string) {
    return this.dbClient.order.findUnique({
      where: {
        id,
      },
      include: {
        ProductToOrder: {
          select: {
            amount: true,
            productId: true,
          },
        },
      },
    });
  }

  async list(page: number, pageSize: number) {
    return this.dbClient.order.findMany({
      skip: page * pageSize,
      take: pageSize,
    });
  }
}

export const orderRepositoryInstance = new OrderRepository();
