import defaultPrismaClient from "../utils/db";
import { PrismaClient } from "@prisma/client";
import { UserModel } from "../model/user";

export class UserRepository {
  constructor(private dbClient: PrismaClient = defaultPrismaClient) {}

  async add(user: UserModel) {
    return this.dbClient.user.create({
      data: {
        dob: new Date(user.dob),
        email: user.email,
        name: user.name,
        photo: user.photo,
        address: user.address,
        latitude: user.latitude,
        longitude: user.longitude,
      },
    });
  }

  async delete(id: string) {
    return this.dbClient.user.delete({
      where: {
        id,
      },
    });
  }

  async update(id: string, user: Partial<UserModel>) {
    return this.dbClient.user.update({
      where: {
        id,
      },
      data: {
        ...(user.dob ? { dob: new Date(user.dob) } : {}),
        ...(user.email ? { email: user.email } : {}),
        ...(user.name ? { name: user.name } : {}),
        ...(user.photo ? { photo: user.photo } : {}),
        ...(user.latitude ? { latitude: user.latitude } : {}),
        ...(user.longitude ? { longitude: user.longitude } : {}),
      },
      select: {
        id: true,
      },
    });
  }

  async get(id: string) {
    return this.dbClient.user.findUnique({
      where: {
        id,
      },
    });
  }

  async list(page: number, pageSize: number) {
    return this.dbClient.user.findMany({
      skip: page * pageSize,
      take: pageSize,
    });
  }
}

export const userRepositoryInstance = new UserRepository();
