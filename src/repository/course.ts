import defaultPrismaClient from "../utils/db";
import { PrismaClient } from "@prisma/client";
import { CourseModel } from "../model/course";

export class CourseRepository {
  constructor(private dbClient: PrismaClient = defaultPrismaClient) {}

  async add(course: CourseModel) {
    return this.dbClient.course.create({
      data: course,
    });
  }

  async delete(id: string) {
    return this.dbClient.course.delete({
      where: {
        id,
      },
    });
  }

  async update(id: string, course: Partial<CourseModel>) {
    return this.dbClient.course.update({
      where: {
        id,
      },
      data: course,
    });
  }

  async get(id: string) {
    return this.dbClient.course.findUnique({
      where: {
        id,
      },
    });
  }

  async list(page: number, pageSize: number) {
    return this.dbClient.course.findMany({
      skip: page * pageSize,
      take: pageSize,
    });
  }
}

export const courseRepositoryInstance = new CourseRepository();
