import defaultPrismaClient from "../utils/db";
import { PrismaClient } from "@prisma/client";
import { PreProductModel } from "../model/product";

export class ProductRepository {
  constructor(private dbClient: PrismaClient = defaultPrismaClient) {}

  async add(product: PreProductModel) {
    return this.dbClient.product.create({
      data: product,
    });
  }

  async delete(id: string) {
    return this.dbClient.product.delete({
      where: {
        id,
      },
    });
  }

  async update(id: string, product: Partial<PreProductModel>) {
    return this.dbClient.product.update({
      where: {
        id,
      },
      data: product,
    });
  }

  async get(id: string) {
    return this.dbClient.product.findUnique({
      where: {
        id,
      },
    });
  }

  async list(page: number, pageSize: number) {
    return this.dbClient.product.findMany({
      skip: page * pageSize,
      take: pageSize,
    });
  }

  async updateStock(id: string, delta: number) {
    return this.dbClient.product.update({
      where: {
        id,
      },
      data: {
        stock: {
          increment: delta,
        },
      },
    });
  }
}

export const productRepositoryInstance = new ProductRepository();
