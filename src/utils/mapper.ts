import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { NextFunction, Request, Response } from "express";
import { ZodError, unknown } from "zod";

export type MappedRequest = {
  body?: unknown;
  query: Record<string, string>;
  params: Record<string, string>;
};

export const mapRequest = (req: Request): MappedRequest => {
  return {
    body: req.body,
    query: Object.entries(req.query)
      .filter(([, value]) => value)
      .reduce(
        (curr, [key, value]) => ({
          ...curr,
          [key]: value!.toString(),
        }),
        {}
      ),
    params: req.params,
  };
};

type CleanRequest = (cleanParams: {
  query: Record<string, string>;
  params: Record<string, string>;
  res: Response;
  body?: unknown;
}) => Promise<Response>;

export const secureRequest =
  (clean: CleanRequest) =>
  (req: Request, res: Response, next: NextFunction) => {
    const { body, query, params } = mapRequest(req);
    return clean({ query, params, res, body })
      .then(() => next())
      .catch((err) => {
        console.error("============= ERROR ==============");
        console.error("Internal error", err);
        console.error("============= ERROR ==============");
        console.error("Request body error", req.body);
        console.error("Request query error", req.query);
        console.error("Request params error", req.params);

        if (err instanceof ZodError) {
          return res.status(400).json({
            errors: err.issues.map((issue, idx) => ({
              path: issue.path.join(" -> "),
              error: issue.message,
            })),
          });
        }

        if (err instanceof PrismaClientKnownRequestError) {
          return res
            .status(400)
            .json({ message: err.meta?.cause || err.message });
        }

        return res.status(500).json({ message: "Internal Server Error" });
      });
  };
