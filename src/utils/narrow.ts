export function narrowOrThrow<T>(name: string): T {
  if (name in process.env) {
    return process.env[name] as T;
  } else {
    throw new Error(`${name} not found in env`);
  }
}
