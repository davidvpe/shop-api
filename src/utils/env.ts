import dotenv from "dotenv";
import { narrowOrThrow } from "./narrow";

class Environment {
  port: string;

  constructor() {
    dotenv.config();
    this.port = narrowOrThrow("PORT");
  }
}

const EnvironmentManager = new Environment();

export { EnvironmentManager };
