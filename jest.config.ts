import type { Config } from "@jest/types";
// Sync object

process.env["DATABASE_URL"] = "local";
process.env["PORT"] = "9999";

const config: Config.InitialOptions = {
  verbose: true,
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
  testEnvironment: "node",
};
export default config;
